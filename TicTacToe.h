#pragma once
 
#include <iostream>
#include <string>
 
using namespace std;

class TicTacToe {
    
    private:
    
        char m_board[9];
        int m_numTurns;
        char m_playerTurn;
        char m_winner;
        int three_row[8][3];
        
    
    public:
    
    TicTacToe() {
        for (int i = 0; i < 9; i++) {
            m_board[i] = ' ';
        }
        m_playerTurn = 'O';
        m_numTurns = 0;
        int three_row[8][3] = {
            {0,1,2},
            {0,3,6},
            {0,4,8},
            {3,4,5},
            {6,7,8},
            {1,4,7},
            {2,5,8},
            {2,4,6}
            
        };

        for (int i = 0; i < 8; i++) {
            for (int o = 0; o < 3; o++) {
                this->three_row[i][o] = three_row[i][o];
            }
        }
        
        
        
    }
    
    void DisplayBoard() {
        cout <<  m_board[0] << " | " << m_board[1] << " | " << m_board[2] <<"\n";
        cout << "―   ―   ―\n";
        cout <<  m_board[3]<<" | "<< m_board[4]<<" | "<< m_board[5]<<"\n";
        cout << "―   ―   ―\n";
        cout <<  m_board[6]<<" | "<< m_board[7]<<" | "<< m_board[8]<<"\n";
    }
    
    bool IsOver() {
        m_numTurns++;
        if(!(m_numTurns < 10)) return true;
        for (int i = 0; i < 8; i++) {

            if (m_board[three_row[i][0]] != ' ' &&
                m_board[three_row[i][0]] == m_board[three_row[i][1]] &&
                m_board[three_row[i][0]] == m_board[three_row[i][2]]) 
            {
                m_winner = m_playerTurn;
                return true;
            }
        }
   
        if(m_playerTurn == 'X') {
        m_playerTurn = 'O';
        } else {
        m_playerTurn = 'X';
        }
        
        return false;
    }
    
    char GetPlayerTurn() {
        return m_playerTurn;
    }
    
    bool IsValidMove(int position) {
        if (m_board[position-1] == ' ') {
            return true;
        } else {
            return false;
        }
    }
    
    void Move(int position) {
        m_board[position-1] = m_playerTurn;
    }
    
    void DisplayResult() {
        if (m_winner != 'X' && m_winner != 'O') {
            cout << "The Game has ended in a tie\n";
        } else {
            cout << "Victory for Player " << m_winner << "\n";
        }
    }
};